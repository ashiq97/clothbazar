﻿using ClothBazar.Entities;
using ClothBazar.Services;
using ClothBazar.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
    public class ProductController : Controller
    {
        ProductsService productsService = new ProductsService();
        // GET: Product
        public ActionResult Index()
        {                
            return View();
        }
        public ActionResult ProductTable(string search)
         {
            var products = productsService.GetProducts();
            if (string.IsNullOrEmpty(search) == false)
            {
                products = products.Where(x => x.Name != null && x.Name.ToLower().Contains(search.ToLower())).ToList();
            }

            return PartialView(products);
        }
       
        [HttpGet]
        public ActionResult Create()
        {
            var categoryService = new CategoriesService();
            
            var categories = categoryService.GetCategories();
            
            return PartialView(categories);
        }

        [HttpPost]
        public ActionResult Create(NewCategoryViewModels model)
        {
            var categoryService = new CategoriesService();

            var newProduct = new Product();
            newProduct.Name = model.Name;
            newProduct.Description = model.Description;
            newProduct.Price = model.Price;
            newProduct.Category = categoryService.GetCategory(model.CategoryId);
            
            productsService.SaveProduct(newProduct);

            return RedirectToAction("ProductTable");
        }

        public ActionResult Edit(int Id)
        {
            var product = productsService.GetProduct(Id);

            return PartialView(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            productsService.UpdateProduct(product);
            return RedirectToAction("ProductTable");
        }


        [HttpPost]
        public ActionResult Delete(int Id)
        {
            productsService.DeleteProduct(Id);
            return RedirectToAction("ProductTable");
        }
    }
}